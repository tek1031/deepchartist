#coding:utf-8
import matplotlib.pyplot as plt

from dataProcessor import dataReader
from dataProcessor import dataCreatorForRegression
from dataProcessor import dataCreatorForClassifier

from neuralNet import DeepChartistNet
from neuralNet import DeepChartistRegression
from neuralNet import MyClassifier
from neuralNet import Regression
import pickle
import time

featureSize = 300
batchsize = 30
n_epoch = 2000
waitTime = 50
dataInterval = 1

trainData = []
testData = []
for year in range(2000, 2014):
	exchangeData = dataReader.readData("USDJPY.csv","USD", year)
#	labeledData = dataCreatorForClassifier.createData(exchangeData, featureSize, waitTime, 0.5)
#	trainData.extend(labeledData)
#	dataForRegression = dataCreatorForRegression.createDataRiseInRange(exchangeData, featureSize,waitTime) 
	dataForRegression = dataCreatorForRegression.createDataMeanInRange(exchangeData, featureSize,waitTime, interval = dataInterval) 
	trainData.extend(dataForRegression)

for year in range(2014, 2016):
	exchangeData = dataReader.readData("USDJPY.csv","USD", year)
	#labeledData = dataCreatorForClassifier.createData(exchangeData, featureSize, waitTime, 0.5)
	#testData.extend(labeledData)
	#dataForRegression = dataCreatorForRegression.createDataRiseInRange(exchangeData, featureSize,waitTime) 
	dataForRegression = dataCreatorForRegression.createDataMeanInRange(exchangeData, featureSize,waitTime, interval = dataInterval) 
	testData.extend(dataForRegression)

dataCreatorForRegression.PlotScatter(testData)


#model = MyClassifier.SoftMaxClassifier(DeepChartistNet.DeepChartist())
model = Regression.Regression(DeepChartistRegression.DeepChartistRegression())
model.setup(trainData, testData)

trainLossHistory = []
testLossHistory = []
testAccuracy = []

for i in range(0, n_epoch):
	start = time.time()
	print('epoch {}:'.format(i))
	trainLoss = model.train(batchsize)
	if(i >=1):	#1回目大きすぎてグラフが崩れるので
		trainLossHistory.append(trainLoss)
	plt.plot(trainLossHistory, label = "x", color = "red")
	fileName = "lossHistory.png"
	plt.savefig(fileName)
	plt.clf()

	testLoss = model.Evaluate(batchsize)
	if(i >=1):	#1回目大きすぎてグラフが崩れるので
		testLossHistory.append(testLoss)
	plt.plot(testLossHistory, label = "x", color = "red")
	fileName = "testHistory.png"
	plt.savefig(fileName)
	plt.clf()

	elapsed_time = time.time() - start
	print ("elapsed_time:{0}".format(elapsed_time)) + "[sec]"

	model.PlotScatter("currentResult", "loss:" + str(testLoss), batchsize)
	model.PlotScatter("epoch-" + str(i),"loss:" + str(testLoss), batchsize)
	if(i%10 == 0):
		pickle.dump(model, open("model-" +str(i), 'wb'), -1)

pickle.dump(model, open("finalModel", 'wb'), -1)
