﻿#coding:utf-8
import chainer.links as L
import chainer
from chainer.functions import softmax
import numpy as np
from chainer import optimizers
from chainer.functions.loss import softmax_cross_entropy
import six

class SoftMaxClassifier(L.classifier.Classifier):
	"""
	ソフトマックスを使った分類
	ChainerのClassifierはなぜかロス率と精度しか返さないので、結果も返す
	結果は確率のはず
	"""

	def __init__(self, predictor):
		super(SoftMaxClassifier,self ).__init__(predictor)
		x_train = None
		y_train = None
		x_test = None
		y_test = None
		self.x_train = None
		self.y_train = None
		self.N_train = 0
		self.x_test = None
		self.y_test = None
		self.optimizer = None


	def setup(self, trainData, testData):
		"""
		学習データを入力、変換する
		trainData はDataのリスト
		Dataはfeatureとlabelを持つ
		Args:
			trainData: 学習データのリスト
		"""
		x_train_row = []
		y_train_row = []
		self.N_train = len(trainData)
		assert self.N_train > 0
		featureSize = len(trainData[0].feature)

		for data in trainData:
			assert len(data.feature) == featureSize
			x_train_row.append(data.feature)
			y_train_row.append(data.label)

		self.x_train = np.array(x_train_row, dtype='f')
		self.y_train = np.array(y_train_row, dtype='i')
		self.x_train = self.x_train.reshape(len(self.x_train), 1, 1,featureSize)

		x_test_row = []
		y_test_row = []

		for data in testData:
			x_test_row.append(data.feature)
			y_test_row.append(data.label)

		self.x_test = np.array(x_test_row, dtype='f')
		self.y_test = np.array(y_test_row, dtype='i')
		self.x_test = self.x_test.reshape(len(self.x_test), 1, 1,featureSize)
		
		self.optimizer = optimizers.Adam()
		self.optimizer.setup(self)

	def CalculateResult(self, x):
		"""
		ソフトマックスを使った分類
		ChainerのClassifierはロス率しか返さないので、結果を返すように拡張
		結果は確率（のはず・・・）
		"""
		output = self.predictor(x) 
		result = softmax(output)
		return result

	def inputData(self, trainData, testData):
		"""
		学習データを入力、変換する
		trainData はDataのリスト
		Dataはfeatureとlabelを持つ
		Args:
			trainData: 学習データのリスト
		"""
		x_train_row = []
		y_train_row = []
		for data in trainData:
			x_train_row.append(data.feature)
			y_train_row.append(data.label)

		x_train = np.array(x_train_row, dtype='f')
		y_train = np.array(y_train_row, dtype='i')
		x_train = x_train.reshape(len(x_train), 1, 1,featureSize)

		x_test_row = []
		y_test_row = []

		for data in testData:
			x_test_row.append(data.feature)
			y_test_row.append(data.label)

		x_test = np.array(x_test_row, dtype='f')
		y_test = np.array(y_test_row, dtype='i')
		x_test = x_test.reshape(len(x_test), 1, 1,featureSize)

	def train(self, batchsize):
		perm = np.random.permutation(self.N_train)
		sum_accuracy = 0
		sum_loss = 0
		for i in six.moves.range(0, self.N_train, batchsize):
			x = chainer.Variable(np.asarray(self.x_train[perm[i:i + batchsize]]))
			t = chainer.Variable(np.asarray(self.y_train[perm[i:i + batchsize]]))
			self.optimizer.update(self, x, t)
			sum_loss += float(self.loss.data) * len(t.data)
			sum_accuracy += float(self.accuracy.data) * len(t.data)
		print('train mean loss={}, accuracy={}'.format(sum_loss / self.N_train, sum_accuracy / self.N_train))
		return sum_loss / self.N_train

	def Evaluate(self):
		N_test = len(x_test)
		sum_accuracy = 0
		sum_loss = 0
		x = chainer.Variable(np.asarray(x_test), volatile='on')
		t = chainer.Variable(np.asarray(y_test), volatile='on')
		loss = self(x, t)

		numOutput =[0,0]
		numCorrect = [0,0]
		for i in range(0, N_test):
			output = 0
			if self.y.data[i][0] < self.y.data[i][1]:
				output = 1 
			answer = y_test[i]
			numOutput[output]+=1
			if output==answer:
				numCorrect[output]+=1

		sum_loss += float(loss.data) * len(t.data)
		sum_accuracy += float(self.accuracy.data) * len(t.data)
		print('test  mean loss={}, accuracy={}'.format(sum_loss / N_test, sum_accuracy / N_test))
		if numOutput[1]>0 :
			print("True Positive:" + str(100.0*numCorrect[1]/numOutput[1]) +"%")
			print("num output:"+str(numOutput[1]))
		if numOutput[0]>0 :
			print("True Negative:" + str(100.0*numCorrect[0]/numOutput[0]) +"%")
			print("num output:"+str(numOutput[0]))
		return sum_accuracy/N_test
