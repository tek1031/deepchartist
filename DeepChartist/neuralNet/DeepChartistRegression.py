﻿import chainer
import chainer.functions as F
import chainer.links as L

class DeepChartistRegression(chainer.Chain):
	def __init__(self):
		super(DeepChartistRegression, self).__init__(
			conv1=  F.Convolution2D(1, 30, ksize=(1,10)),
			conv2=  F.Convolution2D(30, 50, ksize=(1,10)),
			conv3=  F.Convolution2D(50, 100, ksize=(1,10)),
			conv4=  F.Convolution2D(300, 500, ksize=(1,5)),
			l1=F.Linear(200, 1000),
			l2=F.Linear(1000, 1),
			l3=F.Linear(1000, 1),
			)

	def __call__(self, x):
		h = F.max_pooling_2d(F.relu(self.conv1(x)), 4)
		h = F.max_pooling_2d(F.relu(self.conv2(h)), 4)
		h = F.max_pooling_2d(F.relu(self.conv3(h)), 4)
#		h = F.max_pooling_2d(F.relu(self.conv4(h)), 3)
		h = F.dropout(F.relu(self.l1(h)), train=True)
#		h = F.dropout(F.relu(self.l2(h)), train=True)
		y = self.l2(h)
#		y = self.l3(h)
#		y = F.reshape(h, (1))
		return y
