#coding:utf-8
import chainer.links as L
from chainer import link
import six
from chainer.functions import softmax
import chainer
import numpy as np
from chainer import function
import chainer.functions
from chainer.functions.loss import mean_squared_error
import DeepChartistRegression
from chainer.functions.evaluation import accuracy
from chainer import optimizers
import numpy as np
import matplotlib.pyplot as plt

class Regression(link.Chain):
	"""
	ソフトマックスを使った分類
	ChainerのClassifierはなぜかロス率と精度しか返さないので、結果も返す
	結果は確率のはず
	"""
	def __init__(self, predictor,lossfun=mean_squared_error.mean_squared_error):
		super(Regression, self).__init__(predictor=predictor)
		self.lossfun = lossfun
		self.y = None
		self.loss = None
		self.x_train = None
		self.y_train = None
		self.N_train = 0
		self.x_test = None
		self.y_test = None
		self.optimizer = None

	def __call__(self, x, t):
		"""Computes the loss value for an input and label pair.

		It also computes accuracy and stores it to the attribute.

		Args:
			x (~chainer.Variable): Input minibatch.
			t (~chainer.Variable): Corresponding groundtruth labels.

		Returns:
			~chainer.Variable: Loss value.

		"""
		self.y = self.predictor(x)
		self.loss = self.lossfun(self.y, t)
		return self.loss

	def setup(self, trainData, testData):
		"""
		学習データを入力、変換する
		trainData はDataのリスト
		Dataはfeatureとvalueを持つ
		Args:
			trainData: 学習データのリスト
		"""
		x_train_row = []
		y_train_row = []
		self.N_train = len(trainData)
		assert self.N_train > 0
		featureSize = len(trainData[0].feature)

		for data in trainData:
			assert len(data.feature) == featureSize
			x_train_row.append(data.feature)
			y_train_row.append(data.value)

		self.x_train = np.array(x_train_row, dtype='f')
		self.y_train = np.array(y_train_row, dtype='f')
		self.x_train = self.x_train.reshape(len(self.x_train), 1, 1,featureSize)
		self.y_train = self.y_train.reshape(len(self.y_train), 1)

		x_test_row = []
		y_test_row = []

		for data in testData:
			x_test_row.append(data.feature)
			y_test_row.append(data.value)

		self.x_test = np.array(x_test_row, dtype='f')
		self.y_test = np.array(y_test_row, dtype='f')
		self.x_test = self.x_test.reshape(len(self.x_test), 1, 1,featureSize)
		self.y_test = self.y_test.reshape(len(self.y_test), 1)
		
		self.optimizer = optimizers.Adam()
		self.optimizer.setup(self)


	def CalculateResult(self, x):
		"""
		回帰をする
		"""
		output = self.predictor(x) 
		result = softmax(output)
		return result

	def train(self, batchsize):
		perm = np.random.permutation(self.N_train)
		sum_accuracy = 0
		sum_loss = 0
		for i in six.moves.range(0, self.N_train, batchsize):
			x = chainer.Variable(np.asarray(self.x_train[perm[i:i + batchsize]]))
			t = chainer.Variable(np.asarray(self.y_train[perm[i:i + batchsize]]))
			self.optimizer.update(self, x, t)
			sum_loss += float(self.loss.data) * len(t.data)
		print('train mean loss={}'.format(sum_loss /self.N_train))
		return sum_loss /self.N_train


	def Evaluate(self, batchsize):
		N_test = len(self.x_test)
		perm = np.random.permutation(N_test)
		sum_accuracy = 0
		sum_loss = 0
		for i in six.moves.range(0, N_test, batchsize):
			x = chainer.Variable(np.asarray(self.x_test[perm[i:i + batchsize]]))
			t = chainer.Variable(np.asarray(self.y_test[perm[i:i + batchsize]]))
			loss = self(x, t)
			sum_loss += float(loss.data) * len(t.data)

		print('test  mean loss={}'.format(sum_loss / N_test))
		return sum_loss / N_test


	def PlotScatter(self, fileName, label, batchsize):
		dataX = []
		dataY = []
		N_test = len(self.x_test)
		perm = np.random.permutation(N_test)
		for i in six.moves.range(0, N_test, batchsize):
			x = chainer.Variable(np.asarray(self.x_test[perm[i:i + batchsize]]))
			t = chainer.Variable(np.asarray(self.y_test[perm[i:i + batchsize]]))
			loss = self(x, t)
			for data in t.data:
				dataX.append(data[0])
			for data in self.y.data:
				dataY.append(data[0])


		maxX = -100000
		minX =  100000
		maxY = -100000
		minY =  100000

		for data in dataY :
			maxY = max(maxY, data)
			minY = min(minY, data)

		for data in dataX :
			maxX = max(maxX, data)
			minX = min(minX, data)

		plt.grid()
		x = np.linspace(minX, maxX)  
		y = x

		plt.plot(x,y,"r-")    
		plt.xlim(minX, maxX)  
		plt.ylim(minY, maxY) 
		plt.title(label)
		plt.xlabel("answer")
		plt.ylabel("output")
		plt.scatter(dataX, dataY, alpha = 0.2)
		print("save :" + fileName)
		plt.savefig(fileName)
		plt.clf()