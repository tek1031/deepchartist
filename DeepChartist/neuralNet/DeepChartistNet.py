﻿import chainer
import chainer.functions as F
import chainer.links as L


class DeepChartist(chainer.Chain):
	def __init__(self):
		super(DeepChartist, self).__init__(
			conv1=  F.Convolution2D(1, 20, ksize=(1,10)),
			conv2=  F.Convolution2D(20, 50, ksize=(1,10)),
#			conv3=  F.Convolution2D(50, 50, ksize=(1,5)),
			l1=F.Linear(50, 100),
			l2=F.Linear(100, 2),
			)

	def __call__(self, x):
		h = F.max_pooling_2d(F.relu(self.conv1(x)), 4)
		h = F.max_pooling_2d(F.relu(self.conv2(h)), 4)
#		h = F.max_pooling_2d(F.relu(self.conv3(h)), 4)
		h = F.dropout(F.relu(self.l1(h)), train=True)
		return self.l2(h)
	def GetResult(self, x):
		self.y = self.predictor(x)
		print self.y
		return self.y
