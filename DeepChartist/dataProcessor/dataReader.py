#coding:utf-8
import csv
#ファイル形式は拾ってきたやつ
#<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>

def __getFileName__(nation, year):
	return nation + str(year) + ".csv"

def readData(allDataFileName, nation, year):
	"""	
	指定した粘土の、分足データを読み込み、日足データに変換する
	その際、過去にデータを読み込んでいて、日足データを保存していた場合は、そちらを読み込む（高速化のため）
	Args:
		allDataFileName:	全てのデータ入っている為替データ
		nation:				通貨名"USD"など
		year:				何年のデータを読み込むか
	Returns:
		指定した年の日足データのリスト
	"""
	try:
		f = open(__getFileName__(nation,year), 'rb')
		print("Read existing data :" + str(year))
		return __readDayDate__(nation, year)
	except IOError:
		#ファイルがなかったので、データを作り直す
		print("Create New Daily Data" + str(year))
		return __convertMinuteDataToDayData__(allDataFileName, nation, year)


def __convertMinuteDataToDayData__(fileName,nation, year):
	"""	
	指定した年の分足データを日足データに変換し、ファイルに保存する
	ファイル名はexchangeData(year).csv
	Args:
		fileName 分速データのCSV　#<TICKER>,<DTYYYYMMDD>,<TIME>,<OPEN>,<HIGH>,<LOW>,<CLOSE>,<VOL>　この形式のみ
		firstYear この年からのデータのみ読み込み、保存する
	Returns:
		日足データのリスト
	"""
	outputFileName = __getFileName__(nation, year)

	f = open(fileName, 'rb')
	dataReader = csv.reader(f)

	output = open(outputFileName, 'w')
	writer = csv.writer(output, lineterminator='\n')
	row = []
	count = 0
	#データの形式で6番目に入っている
	exchangeIdx = 6 
	exchangeData = []
	for row in dataReader:
		count += 1
		if count == 1:
			continue
		thisYear  = int(row[1]) / 10000
		if year != thisYear:
			continue
		time = int(row[2])
		if(time % 10000) == 0:
			exchange = float(row[exchangeIdx])
			exchangeData.append(exchange)
			row = [exchange]
			writer.writerow(row)

	f.close()
	return exchangeData

def __readDayDate__(nation, year):
	"""
	既にある日足データからデータを読み込む　高速化のため
	"""
	fileName = __getFileName__(nation, year)
	f = open(fileName, 'rb')
	allLine = f.read()
	lines = allLine.split('\r\n')
	exchangeData = []
	for line in lines:
		try:
			exchangeData.append(float(line))
		except ValueError:
			pass

	return exchangeData