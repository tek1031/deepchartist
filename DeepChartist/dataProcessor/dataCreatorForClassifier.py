#coding:utf-8

class Data:
	def __init__(self):
		self.feature = []
		self.label = 0

def createData(exchangeData, featureSize, waitTime, threshold, asDiff = False):
	"""	
	為替データの日足データを受け取り、学習データを作成する
	データの形式はfeatureSize日分の為替の値動きの時系列データ
	ラベルの付け方はwaitTimeの間に何円以上値動きしたか
	
	Args:
		exchangeData:	為替データ　float
		histroyRange:	何日間のデータを特徴ベクトルにするか
		waitTime:		その日から何日の間値動きを待つか　この期間内に指定した分だけ値上がりすれば正例となる
		asDiff:			Trueのとき、前日から何円変化したかの時系列データを出力する
						Falseのときはその日の値がそのまま出力
	Returns:
		学習データのリスト
		学習データは特徴ベクトルfeatureとlabelが付いている
	"""
	diff = [0] #???u??a?I0
	for i in range(1, len(exchangeData)):
		diff = exchangeData[i] - exchangeData[i-1]

	for i in range(0, len(exchangeData)):
		if(i + waitTime+1 >= len(exchangeData)):
			continue
		if(i - featureSize < 0 ):
			continue
		maxValue = -1000000
		feature = []
		if asDiff == True:
			feature = diff[i-featureSize:i]
		else:
			feature = exchangeData[i-featureSize:i]

		for j in range(i+1, i+waitTime+1):
			maxValue = max(maxValue, exchangeData[j] - exchangeData[i])

		data = Data()
		data.feature = feature
		if(maxValue > threshold):
			data.label = 1
			numPositive += 1
		else:
			data.label = 0
			numNegative += 1

		allData.append(data)

	return allData
