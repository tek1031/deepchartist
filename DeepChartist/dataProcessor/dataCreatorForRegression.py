#coding:utf-8
import matplotlib.pyplot as plt
import numpy as np

class Data:
	def __init__(self):
		self.feature = []
		self.value = 0

def createDataRiseInRange(exchangeData, featureSize, waitTime, asDiff = False):
	"""	
	回帰用のデータ作成
	為替データの日足データを受け取り、学習データを作成する
	データの形式はfeatureSize日分の為替の値動きの時系列データ

	正解値は、waitTimeの間にどれくらい値上がりしたか
		
	Args:
		exchangeData:	為替データ　float
		waitTime:		その日から何日の間値動きを待つか　この期間内に指定した分だけ値上がりすれば正例となる
		asDiff:			Trueのとき、前日から何円変化したかの時系列データを出力する
						Falseのときはその日の値がそのまま出力
	Returns:
		学習データのリスト
		学習データは特徴ベクトルfeatureと正解値がついている
	"""
	diff = [0] 
	for i in range(1, len(exchangeData)):
		diff = exchangeData[i] - exchangeData[i-1]

	allData = []
	for i in range(0, len(exchangeData), 20):
		if(i + waitTime+1 >= len(exchangeData)):
			continue
		if(i - featureSize < 0 ):
			continue
		feature = []
		if asDiff == True:
			feature = diff[i-featureSize:i]
		else:
			feature = exchangeData[i-featureSize:i]

		maxValue = 0
		for j in range(i+1, i+waitTime+1):
			if asDiff == True:
				maxValue = max(maxValue, exchangeData[j] - exchangeData[i])
			else:
				maxValue = max(maxValue, exchangeData[j])

		data = Data()
		data.feature = feature
		data.value = maxValue
		allData.append(data)
	return allData

def createDataMeanInRange(exchangeData, featureSize, waitTime, asDiff = False, interval = 1):
	"""	
	回帰用のデータ作成
	為替データの日足データを受け取り、学習データを作成する
	データの形式はfeatureSize日分の為替の値動きの時系列データ

	正解値は、waitTimeの間の平均値の差
		
	Args:
		exchangeData:	為替データ　float
		waitTime:		その日から何日の間値動きを待つか　この期間内に指定した分だけ値上がりすれば正例となる
		asDiff:			Trueのとき、前日から何円変化したかの時系列データを出力する
						Falseのときはその日の値がそのまま出力
	Returns:
		学習データのリスト
		学習データは特徴ベクトルfeatureと正解値がついている
	"""
	diff = [0] 
	for i in range(1, len(exchangeData)):
		diff = exchangeData[i] - exchangeData[i-1]

	allData = []
	for i in range(0, len(exchangeData), interval):
		if(i + waitTime+1 >= len(exchangeData)):
			continue
		if(i - featureSize < 0 ):
			continue
		feature = []
		if asDiff == True:
			feature = diff[i-featureSize:i]
		else:
			feature = exchangeData[i-featureSize:i]

		meanValue = 0
		for j in range(i+1, i+waitTime+1):
			if asDiff == True:
				meanValue += exchangeData[j] - exchangeData[i]
			else:
				meanValue += exchangeData[j]
		meanValue /= waitTime

		data = Data()
		data.feature = feature
		data.value = meanValue
		allData.append(data)
	return allData

def PlotScatter(dataList):
	maxCurrentValue = -100000
	minCurrentValue =  100000
	maxMean = -100000
	minMean =  100000

	currentValueList = []
	meanList = []

	for data in dataList :
		maxCurrentValue = max(maxCurrentValue, data.feature[-1])
		minCurrentValue = min(minCurrentValue, data.feature[-1])
		maxMean = max(maxMean, data.value)
		minMean = min(minMean, data.value)
		currentValueList.append(data.feature[-1])
		meanList.append(data.value)

	plt.grid()
	x = np.linspace(minCurrentValue, maxCurrentValue)  
	y = x

	plt.plot(x,y,"r-")    
	plt.xlim(minCurrentValue, maxCurrentValue)  
	plt.ylim(minMean, maxMean) 
	plt.xlabel("exchange")
	plt.ylabel("mean")
	plt.scatter(currentValueList, meanList, alpha = 0.3)

	plt.savefig("plot of data.png")
	plt.clf()